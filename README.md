#### Enviroment
create virtual enviroment and activate it.

install requirements.txt

make directory `conf` near 'manage.py', and create inside it file `config.env`.
###### config.env file should contain :
    SECRET_KEY=your-super-key
    DEBUG=True
    ALLOWED_HOSTS="127.0.0.1, localhost"
    DATABASE_URL=sqlite:///db.sqlite3 # or any of your choice

#### Start
run project `python manage.py runserver`

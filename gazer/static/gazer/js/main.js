'use strict';

const get_url_list = async () => {
    return await fetch("/api/urlstore/")
        .then(res => res.json())
        .then(data => data);
};


const LinkElement = ({item, setArrayToFetch})=> {
    const [loading, setLoading] = React.useState(false);

    const onCheckboxClick = (e) => {
        setArrayToFetch(item.id);
    }

    const check_link = async (e, id) => {
        e.preventDefault();
        setLoading(true);
        return await fetch(`/api/urlstore/${id}`)
            .then(res => res.json())
            .then(data => {
                let button = document.getElementById("btn-chx-"+id);
                button.className = data.status_code === 200 ? "btn btn-success btn-sm disabled" : "btn btn-danger btn-sm disabled";
                document.getElementById("status-"+id).innerText = "Status: " + data.status_code;
                setLoading(false);
            });
    }

    return(
        <div className="row">
            <div className="col-6 form-check">
                <input className="form-check-input" type="checkbox" value="" id={"chbx-" + item.id} onClick={e=>onCheckboxClick(e)}/>
                <label className="form-check-label" htmlFor={"chbx-" + item.id}>
                    {item.uri}
                </label>
            </div>
            <div className="col-3">
                <button className={"btn btn-outline-primary btn-sm"} id={"btn-chx-"+item.id} onClick={(e) => check_link(e, item.id)}>
                    {loading &&
                        <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                    }
                    <span id={"status-"+item.id}>&nbsp; Check separately</span>
                </button>
            </div>
        </div>
    )
}


const Home = ()=> {
    const [loaded, setLoaded] = React.useState(false);
    const [data, setData] = React.useState([]);
    const [linkArray, setLinkArray] = React.useState([]);
    const [loading, setLoading] = React.useState(false);

    React.useEffect(()=>{
        let url_list = get_url_list();
        url_list.then(list => setData(list));
    }, [loaded]);

    const setArrayToFetch = (link_id)=> {
        linkArray.includes(link_id)
            ? setLinkArray(prev => prev.filter(elem=>elem !== link_id))
            : setLinkArray(prev => [...prev, link_id]);
    }

    const check_link_array = async (e) => {
        e.preventDefault();

        const funk = async (linkId) => {
            setLoading(true);
            return await fetch(`/api/urlstore/${linkId}`)
                .then(res => res.json())
                .then(data => {
                    let button = document.getElementById("btn-chx-"+linkId);
                    button.className = data.status_code === 200 ? "btn btn-success btn-sm disabled" : "btn btn-danger btn-sm disabled";
                    document.getElementById("status-"+linkId).innerText = "Status: " + data.status_code;
                    setLoading(false);
                });
        }

        linkArray.forEach((linkId)=>funk(linkId));
    }

    return (
        <div className={"container"}>
            {
                document.cookie.match("csrftoken") !== null
                    ? <div>
                        <h2>Welcome to Gaze App!</h2>
                        <button onClick={()=>{get_url_list();setLoaded(!loaded)}}
                                className="btn btn-primary ">Update URL List
                        </button>
                    </div>
                    : <h2>You need to authenticate</h2>
            }

            {data.length > 0 &&
                <div className="d-grid gap-3">
                    <h4 className="mb-3 mt-5">Choose URLs for check</h4>
                    {data.map((item, ind)=>{
                        return <LinkElement item={item} ind={ind} key={item.id + 2} setArrayToFetch={setArrayToFetch} />
                    })}
                    <button className={"btn btn-outline-dark col-6"} onClick={(e)=>check_link_array(e)} disabled={linkArray.length===0}>
                        {loading &&
                            <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                        }
                        &nbsp; CHECK THE LIST
                    </button>
                </div>
            }
        </div>
    )
}


const App = () => {
    return(
        <Home />
    )
}


ReactDOM.render(<App/>, document.getElementById('root'));
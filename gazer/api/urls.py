from rest_framework.routers import DefaultRouter
from .views import URLStoreViewSet

app_name = 'api'

router = DefaultRouter()
router.register('urlstore', URLStoreViewSet)

urlpatterns = router.urls

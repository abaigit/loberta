from asgiref.sync import sync_to_async
from django.utils.decorators import method_decorator
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from gazer.models import URLStore
from gazer.tasks import run_task_concurrently
from .serializers import URLStoreSerializer


@method_decorator(sync_to_async, name="dispatch")
class URLStoreViewSet(ModelViewSet):
    serializer_class = URLStoreSerializer
    queryset = URLStore.objects.all()

    def retrieve(self, request, *args, **kwargs):
        uri = URLStore.objects.get(id=self.kwargs.get("pk"))
        status_code = run_task_concurrently(uri.uri)
        return Response({"status_code": status_code.result(), "link_id": uri.id})

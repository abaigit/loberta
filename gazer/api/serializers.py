from rest_framework import serializers
from gazer.models import URLStore


class URLStoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = URLStore
        fields = ["id", "uri", "created"]
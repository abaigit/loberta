from django.shortcuts import render
from asgiref.sync import sync_to_async

@sync_to_async
def home(request):
    return render(request, 'gazer/index.html', {})

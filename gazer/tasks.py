import requests
import asyncio


async def fetch_status_code(URL):
    try:
        response = requests.get(URL, allow_redirects=True)
        response.raise_for_status()

    except requests.exceptions.HTTPError as e:
        return e.response.status_code

    except requests.exceptions.ConnectionError:
        return 404

    return response.status_code


async def set_task(URL):
    return asyncio.create_task(fetch_status_code(URL))


def run_task_concurrently(url):
    return asyncio.run(set_task(url))

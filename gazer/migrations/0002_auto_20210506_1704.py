# Generated by Django 3.2.1 on 2021-05-06 11:04

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('gazer', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='urlstore',
            options={'ordering': ['-updated']},
        ),
        migrations.AddField(
            model_name='urlstore',
            name='created',
            field=models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='Created'),
        ),
        migrations.AddField(
            model_name='urlstore',
            name='updated',
            field=models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='Updated'),
        ),
    ]

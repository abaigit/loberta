from django.apps import AppConfig


class GazerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gazer'

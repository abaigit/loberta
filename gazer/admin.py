from django.contrib import admin
from .models import URLStore

@admin.register(URLStore)
class URLStoreAdmin(admin.ModelAdmin):
    list_display = ['uri', 'created', 'updated']
